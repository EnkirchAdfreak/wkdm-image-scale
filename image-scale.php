<?php
/*
Plugin Name: WKDM custom Image scale
Description: Skaliert die Bilder der Inserate regelmäßig
Version: 1.0
Author: David Leusch
Author URI: http://www.wir-kaufen-dein-motorrad.de
*/

/*
* Author: David Leusch
* License: Use only with explicit permission of Author, no derivatives, no duplication
*/


// create a scheduled event (if it does not exist already)
function cronstarter_activation() {
	if( !wp_next_scheduled( 'wkdm-img-scale-cron' ) ) {
	   wp_schedule_event( time(), 'hourly', 'wkdm-img-scale-cron' );
	}
}
// and make sure it's called whenever WordPress loads
add_action('wp', 'cronstarter_activation');

// unschedule event upon plugin deactivation
function cronstarter_deactivate() {
	// find out when the last event was scheduled
	$timestamp = wp_next_scheduled ('wkdm-img-scale-cron');
	// unschedule previous event if any
	wp_unschedule_event ($timestamp, 'wkdm-img-scale-cron');
}
register_deactivation_hook (__FILE__, 'cronstarter_deactivate');


// here's the function we'd like to call with our cron job
function my_repeat_function() {
    
    //ME: 13.11.2020: $max_leads von 5 auf 10
    //ME: 16:11.2020: $args loop um leads zu holen, von -1, auf 10 gstellt. 
    //ME: 17:11.2020: Max Posts von query loop von einer fixen Zahl auf 1 woche gestellt
    //ME: 17:11.2020: Update Post Meta von immer setzen, in wenn überhaupt Bilder generiert werden gesetzt
    // - Grund dafür: lead wird ohne Bilder erstellt, Cronjob setzt "Images-generated" tag, dann nachträglicher Upload, diese Bilder würden nie erstellt werden
    // - add_post_meta($lead['leadID'], 'images-generated', 'true');
  
    

    // Testing / Debug
    // Image scaling via imagescale() and incresed server memery limit
    $ds         = DIRECTORY_SEPARATOR;
    $wp_content = dirname(dirname(dirname( __FILE__ )));
    
    
    //-----------------------------------
    // Log File
    // file_put_contents
    // deaktiviert 24.9.24
    //-----------------------------------
    //$logfile = $wp_content.$ds.'cron-debug'.$ds.'log.txt';
    //$current = file_get_contents($logfile);


	// Target widths
	$width_sm = 400;
	$width_md = 1000;
	$width_lg = 2000;

	// Number of max lead with generated images at the same time
	$max_leads = 50;
	$i = 0;
    
    

    

	// Get all Leads
    /*
	$args = array(
	    'post_type'         => 'lead',
	    'posts_per_page'    => -1
    );

    */
    
    $args = array(
	    'post_type'         => 'lead',
        'posts_per_page'    => -1,
        // Using the date_query to filter posts from last week
        'date_query' => array(
            array(
                'after' => '1 days ago'
            )
        ),
        'meta_query' => array(
            '0' => array(
                'key' => 'images-generated', 
                'compare' => 'NOT EXISTS'
            )
        )
            
    );
    
    
	$loop = new WP_Query( $args );
	$leads = array();
    
    
    
    $headers = [];
    $headers[] = 'From: WIR KAUFEN DEIN MOTORRAD '.'<angebot@wir-kaufen-dein-motorrad.de>';
    $headers[] = 'Reply-To: angebot@wir-kaufen-dein-motorrad.de';
    //$headers[] = 'Bcc: enkirch@adfreak.de';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'X-Mailer: PHP/' . phpversion();      

    $string_array = json_encode($loop);
    $totest2 = "enkirch@adfreak.de";
    $totestsubject2 = "CronjobBilderResize";
    $totestmessage2 = "loop args2: ".$string_array;
    //wp_mail( $totest2, $totestsubject2, $totestmessage2);
    

	while ( $loop->have_posts() ) : $loop->the_post();

	    // Get all leads without optimized images

	    $meta = get_post_meta(get_the_id());
    
        $imagesGenerated_Exists = metadata_exists('post', get_the_id(), 'images-generated'); // Boolean

	    //if($meta['images-generated'] != "") {
        if( $imagesGenerated_Exists == true ) {    
	        // echo 'images were generated <br>';
	    } else {

	        // echo 'images were not generated <br>';

	        $meta['leadID'] = get_the_id();
	        $leads[] = $meta;

	        $i++;
	        if ($i >= $max_leads) {
	            break;
	        }
	    }
	endwhile;



 
    

	// For each Lead
	if(!empty ($leads)){

		foreach ($leads as $lead) {
		    // Get all images
            /*
            // Undefined array key 0 in D:\xampp-8-2-12\htdocs\wkdm\wp-content\plugins\wkdm-image-scale\image-scale.php on line 148
		    $i = 1;
		    while (get_post_meta( $lead['leadID'], 'images-'.$i )[0] != "") {
		        $images[$i] = get_post_meta( $lead['leadID'], 'images-'.$i )[0];
		        $i++;
		    }
            */
            
            $i = 1;
            while( metadata_exists("post", $lead['leadID'], 'images-'.$i ) ) {
		        $images[$i] = get_post_meta( $lead['leadID'], 'images-'.$i )[0];
		        $i++; 
            };            

            

            
            
		    // For each image
		    if(!empty($images)) {
		        foreach ($images as $image) {
                    
                    
                    //$yearFolder = date("Y");   
                    //$monthFolder = date("m");
                    
                    
                    $pathSegments = parse_url($image, PHP_URL_PATH);
                    $segments = explode('/', rtrim($pathSegments, '/'));
                    $countSegments = count($segments) - 1;

                    $segmentsYear = $segments[$countSegments - 3];
                    $segmentsMonth = $segments[$countSegments - 2];
                    $segmentsOrdner = $segments[$countSegments - 1];                    
                    
                    

                    
                    
		            // echo $image . '<br>';
		            // encode possible spaces in url
		            $image_esc = preg_replace("/ /", "%20", $image);

		            // Get Elements of this path
		            $pathinfo =  pathinfo($image);
                    $pathinfoExtension =  $pathinfo['extension'];

		            // Because we nee path and not URL
		            $folder = basename(dirname($image));
		            // $file = basename($image);
		            //$target_dirname = $wp_content . $ds . 'user-uploads' . $ds . $folder;
                    

                    $target_dirname = $wp_content . $ds . 'user-uploads-2022' . $ds . $segmentsYear . $ds . $segmentsMonth . $ds . $folder;


		            // SM
		            $target_path = $target_dirname . $ds . $pathinfo['filename'] . '-sm.' . $pathinfo['extension'];
                    
                    if($pathinfoExtension == "png" || $pathinfoExtension == "PNG" ){
                        $imgresource = imagecreatefrompng($image_esc);
                        $imgresource_sm = imagescale($imgresource, $width_sm);
                        imagedestroy($imgresource);
                        imagepng($imgresource_sm, $target_path);
                    }else{
                        $imgresource = imagecreatefromjpeg($image_esc);
                        $imgresource_sm = imagescale($imgresource, $width_sm);
                        imagedestroy($imgresource);
                        imagejpeg($imgresource_sm, $target_path);
                    }

		            imagedestroy($imgresource_sm);

                    

		            // MD
		            $target_path = $target_dirname . $ds . $pathinfo['filename'] . '-md.' . $pathinfo['extension'];
                    
                    
                    if($pathinfoExtension == "png" || $pathinfoExtension == "PNG" ){
                        $imgresource = imagecreatefrompng($image_esc);
                        $imgresource_md = imagescale($imgresource, $width_md);
                        imagedestroy($imgresource);
                        imagepng($imgresource_md, $target_path);
                    }else{
                        $imgresource = imagecreatefromjpeg($image_esc);
                        $imgresource_md = imagescale($imgresource, $width_md);
                        imagedestroy($imgresource);
                        imagejpeg($imgresource_md, $target_path);
                    }

		            imagedestroy($imgresource_md);


		            // LG
		            $target_path = $target_dirname . $ds . $pathinfo['filename'] . '-lg.' . $pathinfo['extension'];
                    
                    if($pathinfoExtension == "png" || $pathinfoExtension == "PNG" ){
                        $imgresource = imagecreatefrompng($image_esc);
                        $imgresource_lg = imagescale($imgresource, $width_lg);
                        imagedestroy($imgresource);
                        imagepng($imgresource_lg, $target_path);
                    }else{
                        $imgresource = imagecreatefromjpeg($image_esc);
                        $imgresource_lg = imagescale($imgresource, $width_lg);
                        imagedestroy($imgresource);
                        imagejpeg($imgresource_lg, $target_path);
                    }

		            imagedestroy($imgresource_lg);
                    
                    

					//$jetzt = date("j.m.Y, H:i");
					//$current .= $jetzt .' - ' . $target_path . "\n";

					//file_put_contents($logfile, $current);
                    
                    
                 
                   
                        
                    $checkifTagExists = metadata_exists('post', $lead['leadID'] , 'images-generated');
            
                    // Check if the custom field has a value.
                    if ( $checkifTagExists == true ) {
                        //tag existiert, nicht nochmal erstellen
                    }else{
                        add_post_meta($lead['leadID'], 'images-generated', 'true');
                    }
                        
                    
                    
                    
		        };
                
                
                // Wenn kleine Bilder generiert werden, eine moegliche statische HTML Seite löschen
                // Wen ein Prelead zu einem Lead wird, gibt es noch keine kleinen Bildgrössen.
                // falls diese Seite gecached wird, verweisst der Slider 14 Tage lang auf die grossen Bilder.
                // Um dazu vermeiden wird eine mögliche Statische HTML Seite hier jetzt gelöscht.
                
                if (function_exists ('wp_cache_post_change')) {

                    $query = array(
                        'post_type'         => 'lead',
                        'p' => $lead['leadID']
                    );

                    $leads_querylink = new WP_Query( $query ); 
                    $finalurl = $leads_querylink->posts[0]->guid;

                    wpsc_delete_url_cache($finalurl);
                    
                    
                    
                    $headers = [];
                    $headers[] = 'From: WIR KAUFEN DEIN MOTORRAD '.'<angebot@wir-kaufen-dein-motorrad.de>';
                    $headers[] = 'Reply-To: angebot@wir-kaufen-dein-motorrad.de';
                    //$headers[] = 'Bcc: enkirch@adfreak.de';
                    $headers[] = 'Content-Type: text/html; charset=UTF-8';
                    $headers[] = 'X-Mailer: PHP/' . phpversion();      

                    $string_array = json_encode($loop);
                    $totest3 = "enkirch@adfreak.de";
                    $totestsubject3 = "CronjobBilderResizeCache";
                    $totestmessage3 = "finalurl: ".$finalurl."<br>";
                    //wp_mail( $totest3, $totestsubject3, $totestmessage3, $headers);                    

                }
                
                
                
                
                
                
		    } else {
		        // echo 'has no images <br>';
				//$jetzt = date("j.m.Y, H:i");
				//$current .= $jetzt . ' - ' . $lead['leadID'] . ' - has no images - ' . "\n";
				//file_put_contents($logfile, $current);

     
                
		    }

		    // Update Lead Meta in any case
		    // add_post_meta($lead['leadID'], 'images-generated', 'true');
            sleep(3);
		}
	}
    
    

    
}
// hook function onto our scheduled event:
add_action ('wkdm-img-scale-cron', 'my_repeat_function');


//
// // add custom interval (Debug)
// function cron_add_minute( $schedules ) {
// 	// Adds once every minute to the existing schedules.
//     $schedules['everyminute'] = array(
// 	    'interval' => 60,
// 	    'display' => __( 'Once Every Minute' )
//     );
//     return $schedules;
// }
// add_filter( 'cron_schedules', 'cron_add_minute' );
